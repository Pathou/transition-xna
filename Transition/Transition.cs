using Microsoft.Xna.Framework;
using System;

namespace Transitions
{
    /// <summary>
    /// Linear:			transition constante
    /// ExpoEaseOut:	transition exponentielle (d�but rapide, fin lente)
    /// ExpoEaseOut:	transition exponentielle (d�but lent, fin rapide)
    /// </summary>
    //public enum TransitionEffect { Linear, ExpoEaseOut, ExpoEaseIn }

    /// <summary>
    /// Linear:			transition constante
    /// SmoothStep:		transition Smooth
    /// Curve:			Courbe personalis�e � partir d'un fichier XML
    /// </summary>
    public enum TransitionEffect { Linear, SmoothStep, Curve }

	public class Transition<T>
	{
		public enum StateTransition { Begin, Pending, Done }

		protected double _time;

		protected double _duration;
		protected StateTransition _state;
		protected Curve _curve;
		protected TransitionEffect _effect;
        protected T _start;
        protected T _destination;

        public StateTransition State { get { return _state; } }
		public TransitionEffect Effect { get { return _effect; } set { if(value != TransitionEffect.Curve) _effect = value; } }

		public bool IsFinished { get { return _state == StateTransition.Done ? true : false; } }


        public Transition(T start, T destination, double duration)
        {
            _start = start;
            _destination = destination;
            _duration = duration;
        }
        public Transition(T start, T destination, double duration, TransitionEffect effect)
        {
            _start = start;
            _destination = destination;
            _duration = duration;
            _effect = effect;
        }

        #region XNA

        public T Update(GameTime gametime)
        {
            _time += gametime.ElapsedGameTime.TotalMilliseconds;

            if (_time >= _duration)
            {
                _state = StateTransition.Done;
                return _destination;
            }
            else
            {
                _state = StateTransition.Pending;
                return this.Interpolate();
            }

        }


        /// <summary>
        /// Effectue la transition inverse
        /// </summary>
        /// <param name="reset">Remet le temps � z�ro et met l'�tat � "Begin"</param>
        public void Mirror(bool reset)
        {
            if (reset) this.Reset();
            T tmp = _start;
            _start = _destination;
            _destination = tmp;
        }

        #endregion

        #region M�thode



        /// <summary>
        /// Interpolation
        /// </summary>
        /// <returns>Retourne le vecteur</returns>
        protected T Interpolate()
        {
            float amount = (float)(_time / _duration);

            var type = typeof(T).ToString();

            if (type == "Microsoft.Xna.Framework.Color")
            {
                Color start = (Color)Convert.ChangeType(_start, typeof(Color));
                Color destination = (Color)Convert.ChangeType(_destination, typeof(Color));

                switch (_effect)
                {
                    case TransitionEffect.Curve:
                        return (T)Convert.ChangeType(Color.Lerp(start, destination, _curve.Evaluate(amount)), typeof(T));
                    default:
                        return (T)Convert.ChangeType(Color.Lerp(start, destination, amount), typeof(T));
                }
            }
            else if (type == "Microsoft.Xna.Framework.Vector2")
            {
                Vector2 start = (Vector2)Convert.ChangeType(_start, typeof(Vector2));
                Vector2 destination = (Vector2)Convert.ChangeType(_destination, typeof(Vector2));

                switch (_effect)
                {
                    case TransitionEffect.SmoothStep:
                        return (T)Convert.ChangeType(Vector2.SmoothStep(start, destination, amount), typeof(T));
                    case TransitionEffect.Curve:
                        return (T)Convert.ChangeType(Vector2.Lerp(start, destination, _curve.Evaluate(amount)), typeof(T));
                    default:
                        return (T)Convert.ChangeType(Vector2.Lerp(start, destination, amount), typeof(T));
                }

            }
            else if (type == "System.Double")
            {
                float start = (float)Convert.ChangeType(_start, typeof(float));
                float destination = (float)Convert.ChangeType(_destination, typeof(float));

                switch (_effect)
                {
                    case TransitionEffect.SmoothStep:
                        return (T)Convert.ChangeType(MathHelper.SmoothStep(start, destination, amount), typeof(double));
                    case TransitionEffect.Curve:
                        return (T)Convert.ChangeType(MathHelper.Lerp(start, destination, _curve.Evaluate(amount)), typeof(double));
                    default:
                        return (T)Convert.ChangeType(MathHelper.Lerp(start, destination, amount), typeof(double));
                }

            }
            else if (type == "System.Single")
            {
                float start = (float)Convert.ChangeType(_start, typeof(float));
                float destination = (float)Convert.ChangeType(_destination, typeof(float));

                switch (_effect)
                {
                    case TransitionEffect.SmoothStep:
                        return (T)Convert.ChangeType(MathHelper.SmoothStep(start, destination, amount), typeof(float));
                    case TransitionEffect.Curve:
                        return (T)Convert.ChangeType(MathHelper.Lerp(start, destination, _curve.Evaluate(amount)), typeof(float));
                    default:
                        return (T)Convert.ChangeType(MathHelper.Lerp(start, destination, amount), typeof(float));
                }

            }
            else if (type == "System.Int32" || type == "System.Int16" || type == "System.Int32")
            {
                float start = (float)Convert.ChangeType(_start, typeof(float));
                float destination = (float)Convert.ChangeType(_destination, typeof(float));

                switch (_effect)
                {
                    case TransitionEffect.SmoothStep:
                        return (T)Convert.ChangeType(MathHelper.SmoothStep(start, destination, amount), typeof(int));
                    case TransitionEffect.Curve:
                        return (T)Convert.ChangeType(MathHelper.Lerp(start, destination, _curve.Evaluate(amount)), typeof(int));
                    default:
                        return (T)Convert.ChangeType(MathHelper.Lerp(start, destination, amount), typeof(int));
                }

            }


            throw new System.Exception("Impossible d'effectuer la transition");

            /*switch (_effect)
            {
                case TransitionEffect.Linear:
                    return Color.Lerp(_start, _destination, amount);
                case TransitionEffect.Curve:
                    return Color.Lerp(_start, _destination, _curve.Evaluate(amount));
                default:
                    return Color.Lerp(_start, _destination, amount);
            }*/
        }


        /// <summary>
        /// Remet la transition � z�ro
        /// </summary>
        public void Reset()
		{
			_time = 0;
			_state = StateTransition.Begin;
		}

		/// <summary>
		/// Termine la transition
		/// </summary>
		public void End()
		{
			_time = _duration;
			_state = StateTransition.Done;
		}

		/// <summary>
		/// Ajoute une interpolation Personalis�
		/// Ajout d'une courbe => transitionVector2t.setCurveEffect(Content.Load<Curve>("Curve"));
		/// </summary>
		/// <param name="curve"></param>
		public void setCurveEffect(Curve curve)
		{
			_curve = curve;
			_effect = TransitionEffect.Curve;
		}

		#endregion
	}
}
